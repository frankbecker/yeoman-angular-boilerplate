


/**
 * @ngdoc function
 * @name jiraProjectApp.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the jiraProjectApp
 */

angular.module('jiraProjectApp')
.controller('AppCtrl', ['$scope' , '$http', 'dataUrl', function($scope, $http, dataUrl) {
    $scope.data = {};
    $scope.board;    
    $scope.boards = [];
    $scope.role;
    $scope.roles = [];
    $scope.issue;
    $scope.issues = {};
    $scope.player;
    $scope.players = [];
    $scope.issue_type;
    $scope.issue_types = [];

    $scope.roleSelectedVote = null;
    $scope.focus_player = null;
    $scope.selected_issue = null;


    $scope.init = function(){
        getData();
    };

    $scope.init();

    $scope.on_select = function(value, model){
        console.log(value);
        console.log(model);
        if(model == "role"){
            $scope.roleSelectedVote = value.role_id; 
        }
        
    };

    $scope.setRoleSelected = function (idSelectedRole , type) {
        idSelectedRole = parseInt(idSelectedRole, 10);
        $scope.roleSelectedVote = idSelectedRole;
    };

    $scope.setIssueSelected = function (issue){
        $scope.selected_issue = issue;
        console.log($scope.selected_issue);
        var issue_id = issue.issue_id;
        var player = issue.assignee;
        var team = $scope.data.team_members;
        if(player == "None")return;
        var team_member = _.findWhere(team , {full_name:  player});
        console.log(team_member);
    };

    $scope.showorHide = function(player_full_name){
        if(player_full_name == $scope.focus_player){
            return true;
        }else{
            return false;
        }
        
    };


    $scope.setTeamMemberSelected = function(team_member){
        $scope.focus_player = team_member.full_name;
        console.log($scope.focus_player);
    };

    function getData (){
        $http.get(dataUrl)
            .success(function (data) {
                $scope.data = data;
                console.log(data);
                populate_data(data);
                $scope.boards = data.boards;
                $scope.issues = data.project_issues;

                convert_format_to_date(data.board_sprints);
            })
            .error(function (error) {
                $scope.data.error = error;
        });
    }


    function convert_format_to_date (branch_obj){
            
        _.each(branch_obj, function  (branch) {

            //("dddd, MMMM Do YYYY, h:mm:ss a");
            var date = new Date(); 

            date = Date.parse(branch.startDate);
            date = new Date(date);
            //timestamp: first_value_to_push.getTime(),

            date.setHours(00,00,00);
            branch.start_date_obj = date;
            branch.start_date_obj_timestamp = branch.start_date_obj.getTime();
            

            date = Date.parse(branch.endDate);
            date = new Date(date);

            date.setHours(00,00,00);
            branch.end_date_obj = new Date(date);
            branch.end_date_obj_timestamp = branch.end_date_obj.getTime();



            //branch.start_date_obj = moment(branch.startDate).format('DD/Mm/yy h:mm a');

        // "startDate": "26/Feb/14 6:03 PM",
        //"endDate": "05/Mar/14 6:03 PM",
            
        });


    };     

    function populate_data(data){
        data.roles = [
        {
        role_id : 1,
        role_title : "Business",
        role_team : [
            "Rick"
            ]
        },
        {
        role_id : 2,
        role_title : "Dev",
        role_team : [
            "Akash Gupta", "Kathiresan Ramasamy", "Rahul Sharma", "Ashish Sisodiya", "Joshua Patterson", "Navendhu Mishra"
            ]
        },
        {
        role_id : 3,
        role_title : "Back-End",
        role_team : [
            "Rahul Sharma", "Ashish Sisodiya", "Navendhu Mishra"
            ]
        },
        {
        role_id : 4,
        role_title : "Front-End",
        role_team : [
            "Akash Gupta", "Frank Becker", "Joshua Patterson"
            ]
        },
        {
        role_id : 5,
        role_title : "QA",
        role_team : [
            "Kathiresan Ramasamy"
            ]
        },
        {
        role_id : 6,
        role_title : "Release",
        role_team : [
            "Ganesh"
            ]
        }
        ];

        data.team_members = [
            {
                nick_name: 'Akash',
                full_name: 'Akash Gupta',
                player_id: 1,
                role_id: [
                    2,4
                ]
            },
            {
                nick_name: 'Kathiresan',
                full_name: 'Kathiresan Ramasamy',
                player_id: 2,
                role_id: [
                    5
                ]
            },
            {
                nick_name: 'Rahul',
                full_name: 'Rahul Sharma',
                player_id: 3,
                role_id: [
                    2,3
                ]
            },
            {
                nick_name: 'Ashish',
                full_name: 'Ashish Sisodiya',
                player_id: 4,
                role_id: [
                    2,3
                ]
            },
            {
                nick_name: 'Joshua',
                full_name: 'Joshua Patterson',
                player_id: 5,
                role_id: [
                    2,4
                ]
            },
            {
                nick_name: 'Navendhu',
                full_name: 'Navendhu Mishra',
                player_id: 6,
                role_id: [
                    2,3
                ]
            },
            {
                nick_name: 'Rick',
                full_name: 'Rick James',
                player_id: 7,
                role_id: [
                    1
                ]
            },
            {
                nick_name: 'Ganesh',
                full_name: 'Ganesh Cool',
                player_id: 8,
                role_id: [
                    6
                ]
            }
        ]

        $scope.roles = data.roles;
        /// Get players
        //console.log(data.project_issues);
        _.each(data.project_issues, function(issue){            
            $scope.players.push(issue.assignee);
            $scope.issue_types.push(issue.issue_type);
        });
        $scope.players = _.uniq($scope.players);
        $scope.issue_types = _.uniq($scope.issue_types);
        //console.log($scope.players);
    }
}]);