'use strict';

/**
 * @ngdoc function
 * @name jiraProjectApp.controller:TimeLineCtrl
 * @description
 * # TimeLineCtrl
 * Controller of the jiraProjectApp
 */

angular.module('jiraProjectApp')
.controller('TimeLineCtrl', ['$scope', function($scope) {
    $scope.days_in_year=[];
    
    // this is the target sprint 
    $scope.target_sprint = {
      target_date:"",
      id: "",
      sprint_name: "Welcome",
      startDate : "",
      endDate : "",
      state : "",
      timestamp : ""

    };

    // selected day
    $scope.target_day = '';

    var todays_date = new Date();
    todays_date.setHours(0,0,0,0)
    var today_timestamp = todays_date.getTime();
    $scope.todays_date = {};
    $scope.exampleData = [
    { key: "One", y: 5 },
       { key: "Two", y: 2 },
       { key: "Three", y: 9 },
       { key: "Four", y: 7 },
       { key: "Five", y: 4 },
       { key: "Six", y: 3 },
       { key: "Seven", y: 9 }
    ]; 

    $scope.setTodaysDate = function(timestamp){
        var todays_date = new Date(timestamp);
        $scope.todays_date = {
            date : todays_date,
            timestamp : timestamp,
            formatted : moment(todays_date).format("MMM D, YYYY"),
            day_of_weeek : moment(todays_date).format("dddd")
        };
        $scope.$apply();
    };

    //$scope.setTodaysDate(today_timestamp);

    $scope.init = function(){
        $scope.days_in_year = $scope.getDaysArray();

        //$scope.set_branches_in_year_array();

        setTimeout(function(){
                var $today = $('#'+today_timestamp);
                var $calendar = $('#calendar');
                $today.css("background" , "black");
                $calendar.animate({scrollLeft: $today.position().left-375}, 0);
                $calendar.bind('scroll', function (e) {
                    $scope.get_middle_container();
                });
        },0);

        setTimeout(function (){
          $scope.set_branches_in_year_array($scope.data.board_sprints);

          
        },3000)
    };    

    Date.prototype.getWeek = function() {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };

    $scope.get_click = function($event){
       var $this_container = $($event.currentTarget);
       var parentOffset = $this_container.offset();
       //or $(this).offset(); if you really just want the current element's offset
       var relX = $event.pageX - parentOffset.left;
       var relY = $event.pageY - parentOffset.top;
       console.log("X: "+ relX);
       console.log("Y: "+ relY);
            setTimeout(function() {
                  //$this_container.find("#month_container").trigger("mouseover");
                  // Create a new jQuery.Event object with specified event properties.
                  var e = $.Event("mouseover", { pageX: relX, pageY: relY });

                  // trigger an artificial keydown event with keyCode 64
                  $this_container.find("#month_container").trigger( e );         
            }, 0);
       
    };

    $scope.handle_mouseover = function($event){
        console.log($event);
    };

    $scope.getDaysArray = function() {
      var first_day_year = new Date(new Date().getFullYear(), 0, 1);
      var last_day_year = new Date(new Date().getFullYear(), 11, 31);
      var result = [];
      var first_value_to_push = new Date(new Date().getFullYear(), 0, 1);
      var month = first_value_to_push.getMonth()+1;
        result.push({
            date : first_value_to_push,
            week : first_value_to_push.getWeek(),
            week_to_show : "w"+first_value_to_push.getWeek(),
            month: month,
            month_to_show: "m"+month,
            timestamp: first_value_to_push.getTime(),
            formatted : moment(first_value_to_push).format("MMM D, YYYY"),
            class: ""
        });
      var count = 0;
      while (first_day_year < last_day_year) {
        var date_plus_one = first_day_year.setDate(first_day_year.getDate() + 1);
        var new_date = new Date(date_plus_one);
        var month = new_date.getMonth()+1;
        var month_to_show = (result[count].month != month) ? "m"+month : "";
        var week_to_show = (result[count].week != new_date.getWeek()) ? "w"+new_date.getWeek() : "";
        result.push({
                date : new_date,
                week : new_date.getWeek(),
                week_to_show : week_to_show,
                month: month,
                month_to_show: month_to_show,
                timestamp: new_date.getTime(),
                formatted : moment(new_date).format("MMM D, YYYY"),
                class: ""
        });
        count++;
      }
      return result;
    };

    $scope.get_middle_container = function(){
    var divRect = getRectangle($("#line"));
    var tds = $("#month_container span");
    var tdsUnderDiv = [];
        
    for(var i = 0, j = tds.length; i < j; ++i) {

       var currTd = getRectangle($(tds[i]));
        
       currTd.bottom = Math.round(currTd.top + currTd.height);
       currTd.right  = Math.round(currTd.left + currTd.width);
        
       //console.log(currTd);
        
       if(inCoords(currTd.left, currTd.top, divRect) || inCoords(currTd.right, currTd.bottom, divRect)) {
          
          tdsUnderDiv.push(tds[i]);
       }
    }
      var new_timestamp = parseInt(tdsUnderDiv[0].id, 10);
      $scope.setTodaysDate(new_timestamp);
    };

    $scope.set_branches_in_year_array = function(board_sprints) {

      _.each( board_sprints , function (sprint) {

          try{
            var start_date = _.findWhere($scope.days_in_year , { timestamp : sprint.start_date_obj_timestamp });          
            start_date.sprint_start_date_timestamp = sprint.start_date_obj_timestamp;
          }catch(err){

          }
          

          try{
            var end_date = _.findWhere($scope.days_in_year , { timestamp : sprint.end_date_obj_timestamp });
            end_date.sprint_end_date_timestamp = sprint.end_date_obj_timestamp;
          }catch(err){

          }

          try{
            set_sprint_id (
              start_date.sprint_start_date_timestamp,
              end_date.sprint_end_date_timestamp,
              sprint
            );
          }catch(err){

          }          
          //console.log(start_date);
      });

      
    };

    $scope.sprint_target_selected = function (day){

      //$scope.target_sprint
      try{
        $scope.target_day = day.formatted;
        console.log('Today = '+ day.formatted)

        if(day.sprint_array.length == 0)return;
        //console.log(day.sprint_array[0]);      
        var id = day.sprint_array[0];
        $scope.target_sprint = _.findWhere($scope.data.board_sprints, {id: id});
        console.log($scope.target_sprint);        
      }catch(err){

      }
    };


    function set_sprint_id (start_date, end_date, sprint) {

      _.each($scope.days_in_year, function (day) {
          if(day.timestamp >= start_date && day.timestamp <= end_date){

            /// Spritn Ids
            if(!day.sprint_array){
              day.sprint_array = [];
            }
            
            day.sprint_array.push(sprint.id);
            day.sprint_id = day.sprint_array.join(" ");

            /// Sprint State
            if(!day.state_array){
              day.state_array = [];
            }
            
            day.state_array.push(sprint.state);
            day.sprint_state = day.state_array.join(" ");


            if(!day.sprint_name_array){
              day.sprint_name_array = [];
            }
            
            day.sprint_name_array.push(sprint.sprint_name);
            day.sprint_name = day.sprint_name_array.join(" ");                       
          }
      });

      
    };    


    function getRectangle (obj) {

       var off = obj.offset();

       return {
              top: off.top,
              left: off.left,
              height: obj.outerHeight(),
              width: obj.outerWidth()
       };
    }

    function inCoords (x, y, rect) {

            if ((x > rect.left && x < (rect.left + rect.width))
                && (y > rect.top && y < (rect.top + rect.height)))
                return true;

            return false;
    }

}])
.directive('timeLine', function() {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: 'scripts/templates/timeline.html',
        link: function (scope, element, attrs) {
            /// this is a good way of initializing the controller after the template has loaded
            scope.init();
        }
  };
});