// server.js (Express 4.0)
var express        = require('express');
var bodyParser     = require('body-parser');
var app            = express();

app.use(express.static(__dirname + '/dist'));     // set the static files location /public/img will be /img for users
app.use(bodyParser.urlencoded({ extended: false }));   // parse application/x-www-form-urlencoded
app.use(bodyParser.json());   // parse application/json

/// process.env.PORT is a requirement for Heroku
app.listen(process.env.PORT || 8080);
console.log("Port running in : "+process.env.PORT)