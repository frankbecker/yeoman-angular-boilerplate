'use strict';

/**
 * @ngdoc overview
 * @name jiraProjectApp
 * @description
 * # jiraProjectApp
 *
 * Main module of the application.
 */
angular
  .module('jiraProjectApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'nvd3ChartDirectives'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
.constant('dataUrl', '/jira_data.json');
